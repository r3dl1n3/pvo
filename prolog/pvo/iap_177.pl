:- module(pvo_iap_177, []).

/** <module> 177 IAP regiment

Four squadrons of twelve Su-27 interceptors.

Simultaneously describes all four squadrons, 1 through 4, in the
regiment.

*/

regiment(iap_177).

squadron0(Squadron0) :- between(1, 4, Squadron0).

section0(Section0) :- between(1, 4, Section0).

element0(Element0) :- between(1, 4, Element0).

squadron(Squadron0, Squadron) :-
    squadron0(Squadron0),
    regiment(Regiment),
    atomic_list_concat([Regiment, Squadron0], '_', Squadron).

section(Squadron, Section0, Section) :-
    squadron(_, Squadron),
    section0(Section0),
    atomic_concat(Squadron, Section0, Section).

element(Squadron, Section, Element0, Element) :-
    section(Squadron, _, Section),
    element0(Element0),
    atomic_concat(Section, Element0, Element).

regiment_name("177th IAP").

squadron_name(Squadron, Name) :-
    regiment_name(Name0),
    squadron(Squadron0, Squadron),
    ordinal(Squadron0, Squadron_),
    format(string(Name), '~s ~s Sqn', [Name0, Squadron_]).

section_name(Section, Name) :-
    section(Squadron, Section0, Section),
    squadron_name(Squadron, Name0),
    ordinal(Section0, Section_),
    format(string(Name), '~s ~s Section', [Name0, Section_]).

element_name(Element, Name) :-
    element(Squadron, Section, Element0, Element),
    section(Squadron, Section0, Section),
    squadron_name(Squadron, Name0),
    format(string(Name), '~s ~d-~d', [Name0, Section0, Element0]).

ordinal(1, '1st').
ordinal(2, '2nd').
ordinal(3, '3rd').
ordinal(4, '4th').

element_tail(Element, Tail) :-
    element(Squadron, Section, Element0, Element),
    section(Squadron, Section0, Section),
    squadron(Squadron0, Squadron),
    format(string(Tail), '~d~d~d', [Squadron0, Section0, Element0]).

group(_World, Section, group{name:Name}) :-
    section_name(Section, Name).

unit(_World, Element, unit{name:Name}) :-
    element_name(Element, Name).
unit(_World, Element, unit{onboard_num:Tail}) :-
    once(element_tail(Element, Tail)).
unit(_World, Element, unit{type:"Su-27"}) :-
    element(_, _, _, Element).
unit(_World, Element, unit{livery_id:"Lodeynoye pole AFB (177 IAP)"}) :-
    element(_, _, _, Element).
unit(_World, Element, unit{skill:"Client"}) :-
    element(_, _, _, Element).
unit(_World, Element, unit{payload:Payload}) :-
    element(_, _, _, Element),
    payload(Element, Payload).
unit(World, Element, unit{callsign:Callsign}) :-
    callsign(World, Element, Callsign).

payload(_Element, payload{fuel:9400}).
payload(_Element, payload{gun:100}).
payload(_Element, payload{chaff:96}).
payload(_Element, payload{flare:96}).

callsign(_World, Element, callsign{1:Squadron0}) :-
    element(Squadron, _, _, Element),
    squadron(Squadron0, Squadron).
callsign(_World, Element, callsign{2:Section0}) :-
    element(_, Section, _Element0, Element),
    section(_, Section0, Section).
callsign(_World, Element, callsign{3:Element0}) :-
    element(_, _, Element0, Element).

:- listen(Squadron:country(rus),
          squadron(_, Squadron)).

:- listen(Squadron:element(Section, Element),
          (   squadron(_, Squadron),
              element(Squadron, Section, _, Element)
          )).

:- listen(Squadron:group(World, Section, Group),
          (   squadron(_, Squadron),
              group(World, Section, Group)
          )).

:- listen(Squadron:unit(World, Element, Unit),
          (   squadron(_, Squadron),
              unit(World, Element, Unit)
          )).
