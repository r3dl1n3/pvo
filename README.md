# Soviet air defense forces (PVO)

- 177 IAP out of Lodeynoye Pole air base.

## Abbreviations

- IAP: Fighter (Interceptor) Aviation Regiment

## References

- [Troops of National Air Defense (PVO)](https://fas.org/nuke/guide/russia/agency/pvo.htm)
- [List of squadrons and detachments of the Russian Air Force 2007](https://en.wikipedia.org/wiki/List_of_squadrons_and_detachments_of_the_Russian_Air_Force_2007)
- [Red Air Force Reorganization 1938 – 1941](http://www.alternatewars.com/BBOW/Organization/Soviet_AF_Organization.htm)
